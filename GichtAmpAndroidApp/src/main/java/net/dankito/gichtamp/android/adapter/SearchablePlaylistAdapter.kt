package net.dankito.gichtamp.android.adapter

import net.dankito.gichtamp.MusicPlayer
import net.dankito.gichtamp.PlaylistManager
import net.dankito.gichtamp.android.MainActivity
import net.dankito.gichtamp.android.adapter.viewholder.SongViewHolder
import net.dankito.gichtamp.model.FolderPlaylist
import net.dankito.gichtamp.model.Playlist
import net.dankito.gichtamp.model.Song


class SearchablePlaylistAdapter(activity: MainActivity, playlist: Playlist, playlistManager: PlaylistManager, musicPlayer: MusicPlayer)
    : PlaylistAdapter(activity, playlist, playlistManager, musicPlayer) {

    private var searchResult: List<Song>? = null


    override fun bindItemToView(viewHolder: SongViewHolder, item: Song) {
        super.bindItemToView(viewHolder, item)

        val isCurrentSong = item.id == playlistManager.currentSong?.id ||
                (playlist == playlistManager.currentPlaylist && playlist is FolderPlaylist && item.path == playlistManager.currentSong?.path)

        viewHolder.itemView.isActivated = isCurrentSong
    }

    override fun playlistUpdatedOnUiThread() {
        searchResult?.let {
            items = it
        }
        ?: run { super.playlistUpdatedOnUiThread() }
    }


    fun setSearchResultOnUiThread(result: List<Song>) {
        this.searchResult = result

        items = result
    }

    fun clearSearchResultOnUiThread() {
        this.searchResult = null

        playlistUpdatedOnUiThread()
    }

}