package net.dankito.gichtamp.android

import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import net.dankito.gichtamp.MusicPlayerService
import net.dankito.gichtamp.MusicPlayerServiceBinder
import net.dankito.gichtamp.android.di.AppComponent
import net.dankito.gichtamp.android.di.DaggerAppComponent
import net.dankito.gichtamp.di.MusicPlayerComponent
import net.dankito.gichtamp.di.MusicPlayerModule


class GichtAmpApplication : Application() {

    companion object {

        var service: MusicPlayerService? = null
            private set

    }



    override fun onCreate() {
        super.onCreate()

        setupDependencyInjection()

        val intent = Intent(this, MusicPlayerService::class.java)
        startService(intent)
        bindService(intent, musicPlayerServiceConnection, Context.BIND_AUTO_CREATE)
    }

    private fun setupDependencyInjection() {
        val component = DaggerAppComponent.builder()
                .musicPlayerModule(MusicPlayerModule(this.applicationContext))
                .build()

        MusicPlayerComponent.component = component
        AppComponent.component = component
    }


    private val musicPlayerServiceConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, binder: IBinder) {
            val backgroundServiceBinder = binder as MusicPlayerServiceBinder
            service = backgroundServiceBinder.service
        }

        override fun onServiceDisconnected(className: ComponentName) {
            service = null
        }

    }

}