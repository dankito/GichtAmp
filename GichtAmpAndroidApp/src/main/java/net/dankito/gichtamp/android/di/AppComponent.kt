package net.dankito.gichtamp.android.di

import dagger.Component
import net.dankito.gichtamp.android.MainActivity
import net.dankito.gichtamp.di.MusicPlayerComponent
import net.dankito.gichtamp.di.MusicPlayerModule
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(MusicPlayerModule::class))
interface AppComponent : MusicPlayerComponent {

    companion object {
        lateinit var component: AppComponent
    }


    fun inject(mainActivity: MainActivity)

}