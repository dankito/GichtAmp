package net.dankito.gichtamp.android.adapter.viewholder

import android.support.annotation.IdRes
import android.support.v7.widget.RecyclerView
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.list_item_playlist.view.*
import net.dankito.gichtamp.MusicPlayer
import net.dankito.gichtamp.PlaylistManager
import net.dankito.gichtamp.R
import net.dankito.gichtamp.android.MainActivity
import net.dankito.gichtamp.android.dialog.SetTagsForSongDialog
import net.dankito.gichtamp.android.service.AndroidDialogService
import net.dankito.gichtamp.model.PlaylistInfo
import net.dankito.gichtamp.model.Song
import net.dankito.utils.ui.dialogs.ConfirmationDialogButton
import net.dankito.utils.ui.dialogs.ConfirmationDialogConfig


class SongViewHolder(itemView: View, private val activity: MainActivity, private val playlistManager: PlaylistManager, private val musicPlayer: MusicPlayer)
    : RecyclerView.ViewHolder(itemView), View.OnCreateContextMenuListener {

    val txtFilename: TextView = itemView.txtFilename


    var item: Song? = null

    private val dialogService = AndroidDialogService()


    init {
        itemView.setOnCreateContextMenuListener(this)
    }


    override fun onCreateContextMenu(menu: ContextMenu, view: View, menuInfo: ContextMenu.ContextMenuInfo?) {
        activity.menuInflater.inflate(R.menu.menu_list_item_playlist, menu)

        menu.findItem(R.id.mnPlaylists)?.subMenu?.let { playlistsMenu ->
            getCreatedPlaylistInfosSorted().forEachIndexed { index, info ->
                if (playlistManager.isDisplayedPlaylist(info) == false) { // don't enable to add to displayed playlist
                    val item = playlistsMenu.add(0, index, index, info.displayName)
                    item.titleCondensed = info.name

                    setClickListener(item) { addSelectedSongToCreatedPlaylist(info, it) }
                }
            }
        }

        setClickListener(menu, R.id.mnEnqueue) { playlistManager.enqueueSong(it) }
        setClickListener(menu, R.id.mnEnqueueAtTop) { playlistManager.enqueueSongAtTop(it) }
        setClickListener(menu, R.id.mnTags) { setTagsForSong(it) }
        setClickListener(menu, R.id.mnAddToNewPlaylist) { addSelectedPlaylistItemToNewPlaylist(it) }
        setClickListener(menu, R.id.mnFileDelete) { askToDeleteFile(it) }
    }


    private fun setTagsForSong(selectedSong: Song) {
        SetTagsForSongDialog(activity, selectedSong, playlistManager).showDialog()
    }

    private fun addSelectedSongToCreatedPlaylist(playlistInfo: PlaylistInfo, selectedSong: Song) {
        playlistManager.getCreatedPlaylist(playlistInfo)?.let { playlist ->
            playlistManager.addSongToPlaylist(playlist, selectedSong)
        }
    }

    private fun addSelectedPlaylistItemToNewPlaylist(song: Song) {
        activity.createNewPlaylist { newPlaylist ->
            playlistManager.addSongToPlaylist(newPlaylist, song)
        }
    }


    private fun askToDeleteFile(song: Song) {
        val question = activity.getString(R.string.activity_main_ask_user_to_delete_file, song.path.absolutePath)
        dialogService.showConfirmMessageOnUiThread(activity, question, null, ConfirmationDialogConfig()) { selectedOption ->
            if (selectedOption == ConfirmationDialogButton.Confirm) {
                deleteFile(song)
            }
        }
    }

    private fun deleteFile(song: Song) {
        val deletesCurrentSong = song == playlistManager.currentSong

        // TODO: this is not correct as the PlaylistAdapter's playlist may not be the current playlist (e.g. enqueued items or previously played songs)
        playlistManager.deleteFileFromPlaylistAsync(playlistManager.displayedPlaylist, song) {
            activity.runOnUiThread {
                if (deletesCurrentSong) {
                    musicPlayer.playNextSong()
                }
            }
        }
    }


    private fun setClickListener(menu: Menu, @IdRes menuItemId: Int, listener: (Song) -> Unit) {
        menu.findItem(menuItemId)?.let {
            setClickListener(it, listener)
        }
    }

    private fun setClickListener(menuItem: MenuItem, listener: (Song) -> Unit) {
        menuItem.setOnMenuItemClickListener {
            item?.let {
                listener(it)
            }

            true
        }
    }

    private fun getCreatedPlaylistInfosSorted(): List<PlaylistInfo> {
        return playlistManager.createdPlaylistInfosSorted
    }

}