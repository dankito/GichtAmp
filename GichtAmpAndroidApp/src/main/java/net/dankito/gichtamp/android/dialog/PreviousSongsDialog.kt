package net.dankito.gichtamp.android.dialog

import android.support.v7.app.AlertDialog
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.ContextThemeWrapper
import kotlinx.android.synthetic.main.dialog_previous_songs.*
import net.dankito.gichtamp.MusicPlayer
import net.dankito.gichtamp.PlaylistManager
import net.dankito.gichtamp.R
import net.dankito.gichtamp.android.MainActivity
import net.dankito.gichtamp.android.adapter.PlaylistAdapter


class PreviousSongsDialog(private val activity: MainActivity, private val playlistManager: PlaylistManager, private val musicPlayer: MusicPlayer) {

    fun showDialog() {
        val builder = AlertDialog.Builder(activity)

        builder.setView(R.layout.dialog_previous_songs)

        builder.setPositiveButton(R.string.dialog_previous_songs_close, { dialog, _ ->
            dialog.dismiss()
        })

        val dialog = builder.create()
        dialog.show()

        dialog.rcyPreviousSongs?.apply {
            adapter = PlaylistAdapter(activity, playlistManager.previousSongs, playlistManager, musicPlayer)

            addItemDecoration(DividerItemDecoration(ContextThemeWrapper(this.context, R.style.AppTheme), (layoutManager as LinearLayoutManager).orientation))
        }
    }

}