package net.dankito.gichtamp.android.service

import android.app.Activity
import android.support.v7.app.AlertDialog
import android.widget.EditText
import android.widget.Toast
import net.dankito.gichtamp.R
import net.dankito.gichtamp.android.service.extensions.showKeyboardDelayed
import net.dankito.utils.ui.dialogs.ConfirmationDialogButton
import net.dankito.utils.ui.dialogs.ConfirmationDialogConfig


class AndroidDialogService {


    fun showLittleInfoMessage(activity: Activity, infoMessage: CharSequence) {
        activity.runOnUiThread { showLittleInfoMessageOnUIThread(activity, infoMessage) }
    }

    private fun showLittleInfoMessageOnUIThread(activity: Activity, infoMessage: CharSequence) {
        Toast.makeText(activity, infoMessage, Toast.LENGTH_LONG).show()
    }

    fun showInfoMessage(activity: Activity, infoMessage: CharSequence, alertTitle: CharSequence? = null) {
        activity.runOnUiThread { showInfoMessageOnUIThread(activity, infoMessage, alertTitle) }
    }

    private fun showInfoMessageOnUIThread(activity: Activity, message: CharSequence, alertTitle: CharSequence?) {
        val builder = createDialog(activity, message, alertTitle, android.R.drawable.ic_dialog_info)

        buildAndShowDialog(builder)
    }

    fun showConfirmationDialog(activity: Activity, message: CharSequence, alertTitle: CharSequence?, config: ConfirmationDialogConfig, optionSelected: (ConfirmationDialogButton) -> Unit) {
        activity.runOnUiThread { showConfirmMessageOnUiThread(activity, message, alertTitle, config, optionSelected) }
    }

    fun showConfirmMessageOnUiThread(activity: Activity, message: CharSequence, alertTitle: CharSequence?, config: ConfirmationDialogConfig, optionSelected: (ConfirmationDialogButton) -> Unit) {
        val builder = createDialog(activity, message, alertTitle)

        if(config.showNoButton) {
            builder.setNegativeButton(getConfirmButtonText(activity, R.string.action_no, config.noButtonText)) { _, _ -> optionSelected(ConfirmationDialogButton.No) }
        }
        else {
            builder.setNegativeButton(null, null)
        }

        if(config.showThirdButton) {
            builder.setNeutralButton(getConfirmButtonText(activity, R.string.action_cancel, config.thirdButtonText)) { _, _ -> optionSelected(ConfirmationDialogButton.ThirdButton) }
        }

        val confirmButtonText = getConfirmButtonText(activity, if(config.showNoButton) R.string.action_yes else R.string.action_ok, config.confirmButtonText)
        builder.setPositiveButton(confirmButtonText) { _, _ -> optionSelected(ConfirmationDialogButton.Confirm) }

        buildAndShowDialog(builder)
    }

    private fun getConfirmButtonText(activity: Activity, defaultTextResourceId: Int, customText: String?): String {
        customText?.let { return it }

        return activity.getString(defaultTextResourceId)
    }

    fun showErrorMessage(activity: Activity, errorMessage: CharSequence, alertTitle: CharSequence? = null, exception: Exception? = null) {
        activity.runOnUiThread { showErrorMessageOnUIThread(activity, errorMessage, alertTitle, exception) }
    }

    private fun showErrorMessageOnUIThread(activity: Activity, errorMessage: CharSequence, alertTitle: CharSequence?, exception: Exception?) {
        var message = errorMessage.toString()
        exception?.let {
            message += ":\r\n\r\n" + exception.localizedMessage
        }

        val builder = createDialog(activity, message, alertTitle, android.R.drawable.ic_dialog_alert)

        buildAndShowDialog(builder)
    }

    fun askForTextInput(activity: Activity, questionText: CharSequence, alertTitleText: CharSequence? = null, defaultValue: CharSequence? = null,
                        type: InputType = InputType.Text, callback: (Boolean, String?) -> Unit) {
        activity.runOnUiThread { askForTextInputOnUIThread(activity, questionText, alertTitleText, defaultValue, type, callback) }
    }

    private fun askForTextInputOnUIThread(activity: Activity, questionText: CharSequence, alertTitleText: CharSequence?, defaultValue: CharSequence?, type: InputType, callback: (Boolean, String?) -> Unit) {
        val builder = createDialog(activity, questionText, alertTitleText)

        val input = EditText(activity)
        input.inputType = if(type == InputType.Numbers) android.text.InputType.TYPE_CLASS_NUMBER else android.text.InputType.TYPE_CLASS_TEXT
        defaultValue?.let { input.setText(it) }
        builder.setView(input)

        builder.setPositiveButton(android.R.string.ok) { dialog, which ->
            val enteredResponse = input.text.toString()
            callback(true, enteredResponse)

            dialog.cancel()
        }
        builder.setNegativeButton(android.R.string.cancel) { dialog, which ->
            callback(false, null)

            dialog.cancel()
        }

        buildAndShowDialog(builder)

        input.showKeyboardDelayed()
    }



    private fun createDialog(activity: Activity, message: CharSequence, alertTitle: CharSequence?, iconResource: Int? = null): AlertDialog.Builder {
        var builder = AlertDialog.Builder(activity)

        alertTitle?.let { builder = builder.setTitle(it) }

        builder = builder.setMessage(message)

        iconResource?.let { builder.setIcon(it) }

        builder.setNegativeButton(android.R.string.ok, null)

        return builder
    }

    private fun buildAndShowDialog(builder: AlertDialog.Builder) {
        val dialog = builder.create()

        dialog.show()
    }

}