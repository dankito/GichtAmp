package net.dankito.gichtamp.android

import android.Manifest
import android.app.SearchManager
import android.content.Context
import android.content.res.Configuration
import android.graphics.Typeface
import android.os.*
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.NavigationView
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.graphics.drawable.DrawerArrowDrawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.RelativeLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.action_view_playlist_menu_item.view.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.main_bottom_sheet.*
import kotlinx.android.synthetic.main.view_additional_playback_controls.*
import kotlinx.android.synthetic.main.view_current_playback_position.*
import kotlinx.android.synthetic.main.view_current_playback_position_seekbar.*
import kotlinx.android.synthetic.main.view_enqueued_items.*
import kotlinx.android.synthetic.main.view_standard_playback_controls.*
import net.dankito.filechooserdialog.FileChooserDialog
import net.dankito.filechooserdialog.model.FileChooserDialogConfig
import net.dankito.gichtamp.MusicPlayer
import net.dankito.gichtamp.PlaylistManager
import net.dankito.gichtamp.R
import net.dankito.gichtamp.android.adapter.PlaylistAdapter
import net.dankito.gichtamp.android.adapter.SearchablePlaylistAdapter
import net.dankito.gichtamp.android.di.AppComponent
import net.dankito.gichtamp.android.dialog.PreviousSongsDialog
import net.dankito.gichtamp.android.listener.SimpleOnSeekBarChangeListener
import net.dankito.gichtamp.android.model.ScreenType
import net.dankito.gichtamp.android.service.AndroidDialogService
import net.dankito.gichtamp.model.*
import net.dankito.gichtamp.search.IPlaylistItemsSearcher
import net.dankito.utils.android.permissions.PermissionsService
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.schedule


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    companion object {
        private val log = LoggerFactory.getLogger(MainActivity::class.java)
    }


    @Inject
    protected lateinit var musicPlayer: MusicPlayer

    @Inject
    protected lateinit var playlistManager: PlaylistManager

    @Inject
    protected lateinit var playlistItemsSearcher: IPlaylistItemsSearcher


    private val defaultPlaylistFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC)


    private var searchView: SearchView? = null

    private var playlistStateBeforeSearchStarted: Parcelable? = null

    private val dialogService = AndroidDialogService()

    private val permissionsService = PermissionsService(this)


    private lateinit var playlistAdapter: SearchablePlaylistAdapter

    private lateinit var playlistLayoutManager: LinearLayoutManager

    private lateinit var enqueuedItemsAdapter: PlaylistAdapter

    private val updatePlaybackStateTimer = Timer()
    private var updatePlaybackStateTimerTask: TimerTask? = null

    private lateinit var screenType: ScreenType


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AppComponent.component.inject(this)

        setupLogic()

        setupUi()
    }

    private fun setupLogic() {
        setupPlaylistManager()

        musicPlayer.addStateChangedListener { event -> playerStateChanged(event) }
    }

    private fun setupPlaylistManager() {
        playlistManager.addDisplayedPlaylistChangedListener { playlist ->
            runOnUiThread {
                if (this::playlistAdapter.isInitialized) { // on first start up playlistAdapter may not be initialized when DisplayedPlaylistChangedListener fires
                    playlistAdapter.setPlaylistToOnUiThread(playlist)

                    setButtonToggleShuffleState()

                    updateSearchResultIfInSearchMode()
                }
            }
        }

        playlistManager.addPlaylistItemsChangedListener { playlist, isDisplayedPlaylist, _ ->
            runOnUiThread {
                if (isDisplayedPlaylist) {
                    playlistAdapter.setPlaylistToOnUiThread(playlist)

                    updateSearchResultIfInSearchMode()
                }

                updateNavigationMenuItemsOnUiThread(playlist)
            }
        }

        if (permissionsService.isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            playlistManager.setup(defaultPlaylistFolder)
        }
    }

    private fun askForReadExternalStoragePermission(permissionGrantedAction: () -> Unit) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            permissionsService.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, R.string.activity_main_rational_read_external_storage) { _, isPermitted ->
                if(isPermitted) {
                    permissionGrantedAction()
                }
            }
        }
    }

    private fun setupUi() {
        setContentView(R.layout.activity_main)

        val screenTypeString = resources.getString(R.string.screen_type)
        screenType = ScreenType.valueOf(screenTypeString)

        initDrawerToggle()

        this.searchView = findViewById(R.id.searchView)
        searchView?.let {
            initSearchView(it)
        }

        showAppVersion(nav_view)
        nav_view.setNavigationItemSelectedListener(this)

        updateNavigationMenuItemsOnUiThread()

        setupBottomSheet()


        playlistAdapter = SearchablePlaylistAdapter(this, playlistManager.displayedPlaylist, playlistManager, musicPlayer)
        rcyPlaylist.adapter = playlistAdapter
        playlistLayoutManager = rcyPlaylist.layoutManager as LinearLayoutManager

        enqueuedItemsAdapter = PlaylistAdapter(this, playlistManager.enqueuedSongs, playlistManager, musicPlayer)
        rcyEnqueuedItems.adapter = enqueuedItemsAdapter

        playerStateChangedOnUiThread(PlayerStateEvent(musicPlayer.state, playlistManager.displayedPlaylist))

        Handler().postDelayed({ jumpToCurrentPlayingSong() }, 500) // we need to wait till rcyPlaylist is laid out as otherwise its height isn't set yet
    }

    private fun initDrawerToggle() {
        if (showDrawerIcon == false) { // for tablet layouts we don't display drawer icon and keep drawer always open
            btnToggleDrawer.visibility = View.GONE
        }
        else {
            val drawerArrowDrawable = DrawerArrowDrawable(this)
            drawerArrowDrawable.color = ContextCompat.getColor(this, R.color.tintColor)
            btnToggleDrawer.setImageDrawable(drawerArrowDrawable)

            val toggle = ActionBarDrawerToggle(
                    this, drawer_layout, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
            toggle.drawerArrowDrawable = drawerArrowDrawable
            drawer_layout.addDrawerListener(toggle)
            toggle.syncState()

            drawer_layout.addDrawerListener(object : DrawerLayout.SimpleDrawerListener() {

                override fun onDrawerStateChanged(newState: Int) {
                    if (newState == DrawerLayout.STATE_SETTLING && drawer_layout.isDrawerOpen(GravityCompat.START) == false) {
                        drawerIsOpening()
                    }
                }
            })

            val toggleMethod = ActionBarDrawerToggle::class.java.getDeclaredMethod("toggle")
            toggleMethod.isAccessible = true

            btnToggleDrawer.setOnClickListener {
                toggleMethod.invoke(toggle)
            }

            if (playlistManager.folderPlaylistInfos.isEmpty() && playlistManager.createdPlaylistInfos.isEmpty()) { // no playlists created yet
                drawer_layout.openDrawer(Gravity.START) // -> show NavigationView so that user sees how she can add a new playlist
            }
        }
    }

    private fun drawerIsOpening() {
        setPlaylistItemsState()
    }

    private fun setPlaylistItemsState() {
        nav_view.menu?.let { menu ->
            for (i in 0 until menu.size()) {
                val item = menu.getItem(i)

                item.subMenu?.let { playlistsMenu ->
                    for (playlistIndex in 0 until playlistsMenu.size()) {
                        val playlistItem = playlistsMenu.getItem(playlistIndex)

                        playlistItem.actionView?.apply {
                            (tag as? PlaylistInfo)?.let { playlistInfo ->
                                val isDisplayedPlaylist = playlistManager.isDisplayedPlaylist(playlistInfo)
                                txtvwPlaylistName.setTypeface(null, if (isDisplayedPlaylist) Typeface.BOLD else Typeface.NORMAL)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun setupBottomSheet() {
        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.isHideable = false

        skbrCurrentPlaybackPosition.setOnSeekBarChangeListener(SimpleOnSeekBarChangeListener { progress, fromUser ->
            if (fromUser) {
                musicPlayer.setCurrentPositionTo(progress)
            }
        })

        btnPlayPreviousSong.setOnClickListener { musicPlayer.playPreviousSong() }
        btnPlayPause.setOnClickListener { musicPlayer.playOrPause() }
        btnPlayNextSong.setOnClickListener { musicPlayer.playNextSong() }
        btnJumpToCurrentPlayingSong.setOnClickListener { jumpToCurrentPlayingSong() }
        btnToggleShuffle.setOnClickListener { toggleShuffle() }
    }

    private fun showAppVersion(nav_view: NavigationView) {
        try {
            val packageInfo = this.packageManager.getPackageInfo(packageName, 0)
            val version = packageInfo.versionName
            nav_view.getHeaderView(0).findViewById<TextView>(R.id.txtAppVersion).text = version
        } catch (e: Exception) {
            log.error("Could not read application version")
        }
    }

    private fun setFolderPlaylistsMenuItems() {
        nav_view.menu.findItem(R.id.mnLibraryFolders)?.let { libraryFolderPlaylistsMenuItem ->
            for(i in libraryFolderPlaylistsMenuItem.subMenu.size() - 1 downTo 0) { // clear previously added playlists
                val item = libraryFolderPlaylistsMenuItem.subMenu.getItem(i)
                if(item.itemId != R.id.mnAddLibraryFolder) {
                    libraryFolderPlaylistsMenuItem.subMenu.removeItem(item.itemId)
                }
            }

            getFolderPlaylistInfosSorted().forEachIndexed { index, playlistInfo ->
                addPlaylistMenuItem(libraryFolderPlaylistsMenuItem, playlistInfo, index)
            }
        }
    }

    private fun setTagPlaylistsMenuItems() {
        nav_view.menu.findItem(R.id.mnTagPlaylists)?.let { tagPlaylistsMenuItem ->
            for (i in tagPlaylistsMenuItem.subMenu.size() - 1 downTo 0) { // clear previously added playlists
                val item = tagPlaylistsMenuItem.subMenu.getItem(i)
                if(item.itemId != R.id.mnCreateTag) {
                    tagPlaylistsMenuItem.subMenu.removeItem(item.itemId)
                }
            }

            getTagPlaylistInfosSorted().forEachIndexed { index, playlistInfo ->
                addPlaylistMenuItem(tagPlaylistsMenuItem, playlistInfo, index)
            }
        }
    }

    private fun setCreatedPlaylistsMenuItems() {
        nav_view.menu.findItem(R.id.mnCreatedPlaylists)?.let { createdPlaylistsMenuItem ->
            for(i in createdPlaylistsMenuItem.subMenu.size() - 1 downTo 0) { // clear previously added playlists
                val item = createdPlaylistsMenuItem.subMenu.getItem(i)
                if(item.itemId != R.id.mnCreatePlaylist) {
                    createdPlaylistsMenuItem.subMenu.removeItem(item.itemId)
                }
            }

            getCreatedPlaylistInfosSorted().forEachIndexed { index, playlistInfo ->
                addPlaylistMenuItem(createdPlaylistsMenuItem, playlistInfo, index)
            }
        }
    }

    private fun addPlaylistMenuItem(playlistsGroupMenuItem: MenuItem, playlistInfo: PlaylistInfo, index: Int) {
        val playlistItem = playlistsGroupMenuItem.subMenu.add(0, index, index, "")

        playlistItem.setActionView(R.layout.action_view_playlist_menu_item)
        playlistItem.actionView.apply{
            txtvwPlaylistName.text = playlistInfo.displayName
            imgvwEditPlaylist.setOnClickListener { deletePlaylist(playlistInfo) }

            this.tag = playlistInfo
        }

        playlistItem.setOnMenuItemClickListener {
            setDisplayedPlaylistTo(playlistInfo)
            false
        }
    }

    private fun setDisplayedPlaylistTo(playlistInfo: PlaylistInfo) {
        playlistManager.setDisplayedPlaylist(playlistInfo)
    }

    private fun deletePlaylist(playlistInfo: PlaylistInfo) {
        // TODO: ask user first if she really likes to delete this playlist?
        playlistManager.deletePlaylist(playlistInfo)

        updateNavigationMenuItemsAndSearchResultOnUiThread()
    }


    override fun onStop() {
        playlistManager.saveSettings() // curious: onDestroy() may not be fully handled -> save playlist settings here

        super.onStop()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionsService.onRequestPermissionsResult(requestCode, permissions, grantResults)

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    override fun onBackPressed() {
        searchView?.let { searchView ->
            if(searchView.isIconified == false) { // close search view again
                searchView.setQuery("", false)
                searchView.isIconified = true
                return
            }
        }

        if (showDrawerIcon && drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        else {
            super.onBackPressed()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)

        return true
    }

    private fun initSearchView(searchView: SearchView) {
        val searchManager = this.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchManager.getSearchableInfo(this.componentName))

        searchView.setOnQueryTextListener(searchViewTextListener)
        searchView.setOnCloseListener(searchViewClosesListener)

        searchView.setOnSearchClickListener {
            if (searchView.isIconified == false) {
                setShowSearchViewExpanded(true)
            }
        }
    }

    private fun setShowSearchViewExpanded(showExpanded: Boolean) {
        val visibility = if (showExpanded) View.GONE else View.VISIBLE

        if (showDrawerIcon) {
            btnToggleDrawer.visibility = visibility
        }
        txtCurrentPlayingSong.visibility = visibility
        lytStandardPlaybackControl.visibility = visibility
        lytAdditionalPlaybackControls.visibility = visibility

        (searchView?.layoutParams as? RelativeLayout.LayoutParams)?.let { params ->
            if (showExpanded) { // set to alignParentTop instead of below lytStandardPlaybackControl so that is can expand to top and remove fixed with so that it can expand to left
                params.width = ViewGroup.LayoutParams.MATCH_PARENT

                if (screenType == ScreenType.Phone) {
                    params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE)
                    params.addRule(RelativeLayout.BELOW, 0)
                }
            }
            else { // re-set lytStandardPlaybackControl and width to activity_main_playback_buttons_width
                params.width = resources.getDimension(R.dimen.activity_main_playback_buttons_width).toInt()

                if (screenType == ScreenType.Phone) {
                    params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
                    params.addRule(RelativeLayout.BELOW, R.id.lytStandardPlaybackControl)
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
//            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.mnAddLibraryFolder ->  addFolderToLibrary()
            R.id.mnCreateTag -> createNewTag { }
            R.id.mnCreatePlaylist ->  createNewPlaylist {  }
            R.id.mnPreviousSongs -> showPreviousSongs()
        }

        if (showDrawerIcon) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }

        return true
    }


    private fun getFolderPlaylistInfosSorted(): List<PlaylistInfo> {
        return playlistManager.folderPlaylistInfosSorted
    }

    private fun getTagPlaylistInfosSorted(): List<PlaylistInfo> {
        return playlistManager.tagPlaylistInfosSorted
    }

    private fun getCreatedPlaylistInfosSorted(): List<PlaylistInfo> {
        return playlistManager.createdPlaylistInfosSorted
    }


    private fun playerStateChanged(event: PlayerStateEvent) {
        runOnUiThread {
            playerStateChangedOnUiThread(event)
        }
    }

    private fun playerStateChangedOnUiThread(event: PlayerStateEvent) {
        btnPlayPreviousSong.isEnabled = event.canPlayPreviousSong

        if(event.isPlaying) {
            btnPlayPause.setImageResource(android.R.drawable.ic_media_pause)
            skbrCurrentPlaybackPosition.max = musicPlayer.durationInSeconds
            displayPlaybackPositionTime()

            startPlaybackPositionUpdates()
        }
        else {
            btnPlayPause.setImageResource(android.R.drawable.ic_media_play)
        }

        btnPlayNextSong.isEnabled = event.canPlayNextSong

        setButtonToggleShuffleState()
        playlistAdapter.playlistStateChangedOnUiThread()

        val currentSong = playlistManager.currentSong
        if (currentSong != null) {
            txtCurrentPlayingSong.text = currentSong.displayName
        }
        else {
            txtCurrentPlayingSong.text = ""
        }

        if (playlistManager.displayedPlaylist.currentSong != null) {
            btnJumpToCurrentPlayingSong.isEnabled = true
        }
        else {
            btnJumpToCurrentPlayingSong.isEnabled = false
        }
    }

    private fun startPlaybackPositionUpdates() {
        stopPlaybackPositionUpdates()

        updatePlaybackStateTimerTask = updatePlaybackStateTimer.schedule(500, 500) {
            updatePlaybackPosition()
        }
    }

    private fun stopPlaybackPositionUpdates() {
        updatePlaybackStateTimerTask?.cancel()

        updatePlaybackStateTimerTask = null
    }

    private fun updatePlaybackPosition() {
        runOnUiThread { updatePlaybackPositionOnUiThread() }
    }

    private fun updatePlaybackPositionOnUiThread() {
        val currentPosition = musicPlayer.currentPositionInSeconds

        skbrCurrentPlaybackPosition.progress = currentPosition

        displayPlaybackPositionTime()
    }

    private fun displayPlaybackPositionTime() {
        val currentPosition = musicPlayer.currentPositionInSeconds

        val minutes = currentPosition / 60
        val seconds = currentPosition % 60
        txtCurrentPosition.text = String.format("%d:%02d", minutes, seconds)
    }

    private fun toggleShuffle() {
        playlistManager.toggleDisplayedPlaylistPlaysSongsRandomly()

        setButtonToggleShuffleState()
    }

    private fun setButtonToggleShuffleState() {
        btnToggleShuffle.isChecked = playlistManager.displayedPlaylist.playSongsRandomly
    }

    private fun jumpToCurrentPlayingSong() {
        playlistManager.displayedPlaylist.currentSong?.let {
            val index = playlistAdapter.getSongCurrentlyDisplayedPlaylistIndex(it)
            if (index >= 0) {
                val centerOffset = (rcyPlaylist.height - resources.getDimension(R.dimen.list_item_playlist_height)) / 2
                playlistLayoutManager.scrollToPositionWithOffset(index, centerOffset.toInt())
            }
        }
    }

    private fun updateSearchResultIfInSearchMode() {
        searchView?.let { searchView ->
            if (searchView.isIconified == false) {
                searchPlaylistItems(searchView.query.toString())
            }
        }
    }

    private fun addFolderToLibrary() {
        if(permissionsService.isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            addFolderToLibraryWithReadStoragePermissionGranted()
        }
        else {
            askForReadExternalStoragePermission { addFolderToLibraryWithReadStoragePermissionGranted() }
        }
    }

    private fun addFolderToLibraryWithReadStoragePermissionGranted() {
        val fileChooserDialog = FileChooserDialog()

        val config = FileChooserDialogConfig(initialDirectory = (playlistManager.displayedPlaylist as? FolderPlaylist)?.playlistFolder ?: defaultPlaylistFolder,
                permissionToReadExternalStorageRationaleResourceId = R.string.activity_main_rational_read_external_storage)

        fileChooserDialog.showSelectFolderDialog(this, permissionsService, config) { didUserSelectFolder, folder ->
            if(didUserSelectFolder && folder != null) {
                addFolderToLibrary(folder)
            }
        }
    }

    private fun addFolderToLibrary(folder: File) {
        playlistManager.addPlaylistFromFolderAsync(folder) {
            runOnUiThread {
                updateNavigationMenuItemsAndSearchResultOnUiThread()
            }
        }
    }

    private fun updateNavigationMenuItemsAndSearchResultOnUiThread() {
        updateNavigationMenuItemsOnUiThread()

        updateSearchResultIfInSearchMode()
    }

    private fun updateNavigationMenuItemsOnUiThread() {
        setFolderPlaylistsMenuItems()
        setTagPlaylistsMenuItems()
        setCreatedPlaylistsMenuItems()
    }

    private fun updateNavigationMenuItemsOnUiThread(playlist: Playlist) {
        if (playlist is FolderPlaylist) {
            setFolderPlaylistsMenuItems()
        }
        else if (playlist is TagPlaylist) {
            setTagPlaylistsMenuItems()
        }
        else if (playlist is CreatedPlaylist) {
            setCreatedPlaylistsMenuItems()
        }
    }

    fun createNewTag(createdTag: (TagPlaylist) -> Unit) {
        askUserForName(getString(R.string.activity_main_select_tag_name)) { name ->
            val newTagPlaylist = playlistManager.addTagPlaylist(name)
            if (newTagPlaylist != null) {
                setTagPlaylistsMenuItems()
                createdTag(newTagPlaylist)
            }
            else {
                showCouldNotCreateTag(name)
            }
        }
    }

    private fun showCouldNotCreateTag(tagName: String) {
        if(playlistManager.hasTagPlaylistWithName(tagName)) {
            dialogService.showErrorMessage(this, getString(R.string.activity_main_error_message_could_not_create_tag_name_already_exist, tagName))
        }
        else {
            dialogService.showErrorMessage(this, getString(R.string.activity_main_error_message_could_not_tag_playlist, tagName))
        }
    }

    fun createNewPlaylist(createdPlaylist: (CreatedPlaylist) -> Unit) {
        askUserForName(getString(R.string.activity_main_select_playlist_name)) { name ->
            val newPlaylist = playlistManager.addCreatedPlaylist(name)
            if(newPlaylist != null) {
                setCreatedPlaylistsMenuItems()
                createdPlaylist(newPlaylist)
            }
            else {
                showCouldNotCreatePlaylist(name)
            }
        }
    }

    private fun showCouldNotCreatePlaylist(playlistName: String) {
        if(playlistManager.hasCreatedPlaylistWithName(playlistName)) {
            dialogService.showErrorMessage(this, getString(R.string.activity_main_error_message_could_not_create_playlist_name_already_exist, playlistName))
        }
        else {
            dialogService.showErrorMessage(this, getString(R.string.activity_main_error_message_could_not_create_playlist, playlistName))
        }
    }

    private fun askUserForName(prompt: String, selectedName: (String) -> Unit) {
        dialogService.askForTextInput(this, prompt) { didSetValue, name ->
            if(didSetValue && name != null) {
                // TODO: check playlistName for illegal path characters
                selectedName(name)
            }
        }
    }


    private fun showPreviousSongs() {
        PreviousSongsDialog(this, playlistManager, musicPlayer).showDialog()
    }


    /**
     * For tablet layouts in landscape mode we don't display drawer icon and keep drawer always open
     */
    private val showDrawerIcon: Boolean
        get() = screenType != ScreenType.TenInchTablet || isInPortraitMode

    private val isInPortraitMode: Boolean
        get() = resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT


    private val searchViewTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextChange(query: String): Boolean {
            searchPlaylistItems(query)
            return true
        }

        override fun onQueryTextSubmit(query: String): Boolean {
            return true
        }
    }

    private val searchViewClosesListener = SearchView.OnCloseListener {
        playlistStateBeforeSearchStarted?.let { savedState ->
            playlistLayoutManager.onRestoreInstanceState(savedState)

            playlistStateBeforeSearchStarted = null
        }

        setShowSearchViewExpanded(false)

        false
    }


    private fun searchPlaylistItems(query: String) {
        if (playlistStateBeforeSearchStarted == null) {
            playlistStateBeforeSearchStarted = playlistLayoutManager.onSaveInstanceState()
        }

        if (query.isEmpty()) {
            playlistAdapter.clearSearchResultOnUiThread()
        }
        else {
            playlistItemsSearcher.searchAsync(playlistManager.displayedPlaylist, query) {searchResult ->
                playlistAdapter.setSearchResultOnUiThread(searchResult)
            }
        }
    }

}
