package net.dankito.gichtamp.android.adapter

import android.view.View
import net.dankito.gichtamp.PlaylistManager
import net.dankito.gichtamp.R
import net.dankito.gichtamp.android.adapter.items.TagListItem
import net.dankito.gichtamp.android.adapter.viewholder.TagViewHolder
import net.dankito.gichtamp.model.PlaylistInfo
import net.dankito.utils.android.ui.adapter.ListRecyclerAdapter


open class TagsAdapter(protected var tagsForSong: List<PlaylistInfo>, protected val playlistManager: PlaylistManager)
    : ListRecyclerAdapter<TagListItem, TagViewHolder>(getTagListItems(playlistManager, tagsForSong)) {

    companion object {

        private fun getTagListItems(playlistManager: PlaylistManager, tagsForSong: List<PlaylistInfo>): List<TagListItem> {
            return playlistManager.tagPlaylistInfosSorted.map { TagListItem(it, tagsForSong.contains(it)) }
        }

    }


    override fun getListItemLayoutId() = R.layout.list_item_tag

    override fun createViewHolder(itemView: View): TagViewHolder {
        return TagViewHolder(itemView)
    }

    override fun bindItemToView(viewHolder: TagViewHolder, item: TagListItem) {
        viewHolder.chkbxIsSetOnSong.apply {
            isChecked = item.isSetOnSong

            text = item.displayName

            setOnCheckedChangeListener { _, isChecked -> item.isSetOnSong = isChecked }
        }
    }


    fun getTagsSetOnSong(): List<PlaylistInfo> {
        return items.filter { it.isSetOnSong }.map { it.tag }
    }

    open fun tagsHaveBeenUpdated() {
        this.items = getTagListItems(playlistManager, getTagsSetOnSong())
    }

}