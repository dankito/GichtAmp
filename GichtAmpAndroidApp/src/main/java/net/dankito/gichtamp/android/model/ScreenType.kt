package net.dankito.gichtamp.android.model


enum class ScreenType {

    Phone,

    SevenInchTablet,

    TenInchTablet

}