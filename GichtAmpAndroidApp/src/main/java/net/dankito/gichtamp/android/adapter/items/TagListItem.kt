package net.dankito.gichtamp.android.adapter.items

import net.dankito.gichtamp.model.PlaylistInfo


class TagListItem(
    val tag: PlaylistInfo,
    var isSetOnSong: Boolean
) {

    val displayName: String
        get() = tag.displayName

}