package net.dankito.gichtamp.android.adapter.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.CheckBox
import kotlinx.android.synthetic.main.list_item_tag.view.*


class TagViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val chkbxIsSetOnSong: CheckBox = itemView.chkbxIsSetOnSong

}