package net.dankito.gichtamp.android.listener

import android.widget.SeekBar


open class SimpleOnSeekBarChangeListener(
    protected val progressChanged: ((progress: Int, fromUser: Boolean) -> Unit)? = null
) : SeekBar.OnSeekBarChangeListener {

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        progressChanged?.invoke(progress, fromUser)
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {

    }

}