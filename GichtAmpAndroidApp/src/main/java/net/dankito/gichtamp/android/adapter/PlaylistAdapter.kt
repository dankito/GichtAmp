package net.dankito.gichtamp.android.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import net.dankito.gichtamp.MusicPlayer
import net.dankito.gichtamp.PlaylistManager
import net.dankito.gichtamp.R
import net.dankito.gichtamp.android.MainActivity
import net.dankito.gichtamp.android.adapter.viewholder.SongViewHolder
import net.dankito.gichtamp.model.*
import net.dankito.utils.android.ui.adapter.ListRecyclerAdapter


open class PlaylistAdapter(protected val activity: MainActivity, protected var playlist: SongsList,
                           protected val playlistManager: PlaylistManager, private val musicPlayer: MusicPlayer)
    : ListRecyclerAdapter<Song, SongViewHolder>() {

    private val playlistChangedListener = { _: SongsList ->
        activity.runOnUiThread {
            playlistUpdatedOnUiThread()
        }
    }


    init {
        setPlaylistToOnUiThread(playlist)

        itemClickListener = { song -> musicPlayer.playSong(song, playlist) }
    }


    override fun getListItemLayoutId() = R.layout.list_item_playlist

    override fun createViewHolder(itemView: View): SongViewHolder {
        return SongViewHolder(itemView, activity, playlistManager, musicPlayer)
    }

    override fun bindItemToView(viewHolder: SongViewHolder, item: Song) {
        viewHolder.item = item

        viewHolder.txtFilename.apply {
            text = item.displayName
        }
    }


    open fun setPlaylistToOnUiThread(playlist: SongsList) {
        this.playlist.removePlaylistChangedListeners(playlistChangedListener)

        this.playlist = playlist
        this.playlist.addPlaylistChangedListeners(playlistChangedListener)

        playlistUpdatedOnUiThread()
    }

    open fun playlistUpdatedOnUiThread() {
        items = playlist.songs
    }

    fun playlistStateChangedOnUiThread() {
        notifyDataSetChanged()
    }


    fun getSongCurrentlyDisplayedPlaylistIndex(song: Song): Int {
        return items.indexOf(song)
    }


    override fun itemClicked(viewHolder: RecyclerView.ViewHolder, item: Song, position: Int): Boolean {
        super.itemClicked(viewHolder, item, position)

        return false // this seems to be a bug in ListRecyclerAdapter: we have to return false as otherwise also context menu gets displayed
    }

}