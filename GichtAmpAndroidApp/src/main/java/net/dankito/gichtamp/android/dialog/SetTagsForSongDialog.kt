package net.dankito.gichtamp.android.dialog

import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import kotlinx.android.synthetic.main.dialog_set_tags_for_song.*
import net.dankito.gichtamp.PlaylistManager
import net.dankito.gichtamp.R
import net.dankito.gichtamp.android.MainActivity
import net.dankito.gichtamp.android.adapter.TagsAdapter
import net.dankito.gichtamp.model.PlaylistInfo
import net.dankito.gichtamp.model.Song


class SetTagsForSongDialog(private val mainActivity: MainActivity, private val song: Song, private val playlistManager: PlaylistManager) {

    private val currentTagsForSong = playlistManager.getTagsForSong(song)

    private var adapter: TagsAdapter


    var tagsForSongChangedListener: ((Song, addedTags: List<PlaylistInfo>, removedTags: List<PlaylistInfo>) -> Unit)? = null


    init {
        adapter = TagsAdapter(currentTagsForSong, playlistManager)
    }


    fun showDialog() {
        val builder = AlertDialog.Builder(mainActivity)

        builder.setTitle(mainActivity.getString(R.string.dialog_set_tags_for_song_title, song.displayName))

        builder.setView(R.layout.dialog_set_tags_for_song)

        builder.setNegativeButton(android.R.string.cancel, { dialog, _ -> dialog.dismiss() })

        builder.setNeutralButton(R.string.dialog_set_tags_for_song_create_new_tag) { _, _ ->  }

        builder.setPositiveButton(android.R.string.ok, { dialog, _ ->
            setTagsForSong()
            dialog.dismiss()
        })

        val dialog = builder.create()
        dialog.show()

        val neutralButton = dialog.getButton(DialogInterface.BUTTON_NEUTRAL)
        // click listener has to be set after calling .show() as otherwise a click on neutral button would close dialog
        neutralButton.setOnClickListener { createNewTag() }

        dialog.rcyTags?.adapter = adapter
    }


    private fun setTagsForSong() {
        val newTags = adapter.getTagsSetOnSong()

        val addedTags = newTags.filter { currentTagsForSong.contains(it) == false }
        val removedTags = currentTagsForSong.filter { newTags.contains(it) == false }

        addedTags.forEach { addedTag ->
            playlistManager.addSongToPlaylist(addedTag, song)
        }

        removedTags.forEach { removedTag ->
            playlistManager.removeSongFromPlaylist(removedTag, song)
        }

        if (addedTags.isNotEmpty() || removedTags.isNotEmpty()) {
            tagsForSongChangedListener?.invoke(song, addedTags, removedTags)
        }
    }


    private fun createNewTag() {
        mainActivity.createNewTag {
            adapter.tagsHaveBeenUpdated()
        }
    }

}