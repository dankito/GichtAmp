package net.dankito.gichtamp.search

import net.dankito.gichtamp.model.Playlist
import net.dankito.gichtamp.model.Song


interface IIndexedPlaylistItemsSearcher : IPlaylistItemsSearcher {

    fun indexPlaylistAsync(playlist: Playlist)

    fun indexPlaylist(playlist: Playlist)

    fun indexSong(playlist: Playlist, song: Song)

}