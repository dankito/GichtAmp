package net.dankito.gichtamp.model

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import net.dankito.gichtamp.metadata.Metadata
import java.io.File
import java.util.*


@JsonIdentityInfo(generator = ObjectIdGenerators.StringIdGenerator::class,
        property = "id") // to avoid that the same object gets written multiple times to Json file when Song is enqueued
class Song(val path: File, val relativePath: String?, var metadata: Metadata? = null) {

    val id: String = UUID.randomUUID().toString()


    val filename: String
        get() = path.name

    val displayName: String
        get() {
            metadata?.displayName?.let { metadataDisplayName ->
                if (metadataDisplayName.isBlank() == false) {
                    return metadataDisplayName
                }
            }

            return filename
        }


    internal constructor() : this(File("/"), null) // for object deserializers


    override fun toString(): String {
        return displayName
    }

}