package net.dankito.gichtamp.di

import android.content.Context
import dagger.Module
import dagger.Provides
import net.dankito.gichtamp.MusicPlayer
import net.dankito.gichtamp.PlaylistManager
import net.dankito.gichtamp.search.IPlaylistItemsSearcher
import net.dankito.gichtamp.search.PlaylistItemsSearcher
import java.io.File
import javax.inject.Singleton


@Module
class MusicPlayerModule(private val applicationContext: Context) {

    @Provides
    @Singleton
    fun provideApplicationContext() : Context {
        return applicationContext
    }

    @Provides
    @Singleton
    fun providePlaylistItemsSearcher() : IPlaylistItemsSearcher {
        return PlaylistItemsSearcher()
    }

    @Provides
    @Singleton
    fun providePlaylistManager(applicationContext: Context) : PlaylistManager {
        return PlaylistManager(File(applicationContext.filesDir, "data"), null)
    }

    @Provides
    @Singleton
    fun provideMusicPlayer(applicationContext: Context, playlistManager: PlaylistManager) : MusicPlayer {
        return MusicPlayer(applicationContext, playlistManager)
    }

}