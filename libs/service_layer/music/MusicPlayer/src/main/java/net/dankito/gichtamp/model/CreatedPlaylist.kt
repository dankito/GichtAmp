package net.dankito.gichtamp.model


class CreatedPlaylist(name: String) : Playlist(name) {

    internal constructor() : this("") // for object deserializers


    override fun toString(): String {
        return "$name: $size items"
    }

}