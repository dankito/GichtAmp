package net.dankito.gichtamp.model

import com.fasterxml.jackson.annotation.JsonTypeInfo
import java.util.*


@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property="@class")
abstract class Playlist(var name: String) : SongsList() {

    val id = UUID.randomUUID().toString()

    var currentPlaylistPosition = 0
        private set

    @Transient
    var currentSong: Song? = null
        private set

    var playSongsRandomly: Boolean = false


    fun initializeAfterDeserializing(info: PlaylistInfo) {
        this.name = info.name
        this.currentPlaylistPosition = info.currentSongIndex
        this.playSongsRandomly = info.playSongsRandomly

        get(currentPlaylistPosition)?.let { currentSong ->
            this.currentSong = currentSong
        }
    }


    override fun removeSong(song: Song): Boolean {
        val success = super.removeSong(song)

        if (success) {
            if (currentPlaylistPosition >= songsField.size) {
                setPlaylistPosition(currentPlaylistPosition - 1)
            }
        }

        return success
    }


    val canPlayPreviousSong: Boolean
        get() = hasItems && (playSongsRandomly || currentPlaylistPosition > 0)

    fun getPreviouslyPlayedSong(): Song? {
        val previousSongIndex = if (playSongsRandomly) {
            randomlySelectNextSongIndex()
        }
        else {
            currentPlaylistPosition - 1
        }

        return get(previousSongIndex)
    }

    val canPlayNextSong: Boolean
        get() = hasItems && (playSongsRandomly || currentPlaylistPosition < size - 1)

    fun getNextSongToPlay(): Song? {
        val nextSongIndex = if (playSongsRandomly) {
            randomlySelectNextSongIndex()
        }
        else {
            currentPlaylistPosition + 1
        }

        return get(nextSongIndex)
    }

    private fun randomlySelectNextSongIndex(): Int {
        val random = Random(System.nanoTime())

        return random.nextInt(size)
    }


    fun setCurrentSong(newCurrentSong: Song) {
        val index = songsField.indexOf(newCurrentSong)

        if (index >= 0) {
            currentSong = newCurrentSong
            setPlaylistPosition(index)
        }
    }

    private fun setPlaylistPosition(index: Int) {
        if (index >= 0 && index < size) {
            currentPlaylistPosition = index
        }
    }


    fun togglePlaysSongsRandomly() {
        playSongsRandomly = !!! playSongsRandomly
    }

}