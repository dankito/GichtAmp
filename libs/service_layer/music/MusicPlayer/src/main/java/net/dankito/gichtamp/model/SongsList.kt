package net.dankito.gichtamp.model


abstract class SongsList {

    protected val songsField = mutableListOf<Song>()

    @Transient
    protected val playlistChangedListeners = mutableListOf<(SongsList) -> Unit>()


    open val songs: List<Song>
        get() = ArrayList(songsField) // don't pass mutable state to outside

    open val hasItems: Boolean
        get() = songsField.size > 0

    open val size: Int
        get() = songsField.size


    open fun get(index: Int): Song? {
        if(index >= 0 && index < songsField.size) {
            return songsField[index]
        }

        return null
    }

    open fun addAtTop(song: Song) {
        songsField.add(0, song)

        callPlaylistChangedListeners()
    }

    open fun add(song: Song) {
        songsField.add(song)

        callPlaylistChangedListeners()
    }

    open fun removeSong(song: Song): Boolean {
        val successful = songsField.remove(song)

        callPlaylistChangedListeners()

        return successful
    }


    fun addPlaylistChangedListeners(listener: (SongsList) -> Unit) {
        playlistChangedListeners.add(listener)
    }

    fun removePlaylistChangedListeners(listener: (SongsList) -> Unit) {
        playlistChangedListeners.remove(listener)
    }

    protected fun callPlaylistChangedListeners() {
        ArrayList(playlistChangedListeners).forEach { listener ->
            listener(this)
        }
    }


    override fun toString(): String {
        return "$size items"
    }

}