package net.dankito.gichtamp.metadata


open class Metadata(
        var artist: String,
        var title: String,
        var album: String,
        var length: Int? = null
) {

    internal constructor() : this("", "", "") // for object deserializers


    val displayName: String
        get() {
            val displayName = StringBuilder(artist)

            if (artist.isNotBlank() && title.isNotBlank()) {
                displayName.append(" - ")
            }

            displayName.append(title)

            if (album.isNotBlank()) {
                if (displayName.length > 0) {
                    displayName.append(" ")
                }

                displayName.append("(", album, ")")
            }

            return displayName.toString()
        }

    val lengthDisplayValue: String
        get() {
            val lengthInSeconds = length?.let { it / 1000 } ?: 0

            return String.format("%d:%02d", (lengthInSeconds / 60), (lengthInSeconds % 60))
        }


    override fun toString(): String {
        return displayName
    }

}