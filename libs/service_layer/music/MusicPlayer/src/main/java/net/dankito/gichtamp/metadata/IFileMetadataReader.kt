package net.dankito.gichtamp.metadata

import java.io.File


interface IFileMetadataReader {

    fun readMetadata(file: File): Metadata?

}