package net.dankito.gichtamp

import android.os.Binder


class MusicPlayerServiceBinder(val service: MusicPlayerService) : Binder()