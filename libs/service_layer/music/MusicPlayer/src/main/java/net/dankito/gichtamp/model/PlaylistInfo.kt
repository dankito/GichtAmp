package net.dankito.gichtamp.model


data class PlaylistInfo(
        val id: String, val type: PlaylistType, var name: String, var countItems: Int,
        var currentSongIndex: Int, var playSongsRandomly: Boolean
) {

    internal constructor() : this("", PlaylistType.Created, "", 0, 0, false) // for object deserializers


    val displayName: String
        get() = "$name ($countItems)"


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is PlaylistInfo) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }


    override fun toString(): String {
        return displayName
    }


}