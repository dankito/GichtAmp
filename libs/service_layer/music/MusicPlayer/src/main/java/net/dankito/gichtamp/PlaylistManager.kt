package net.dankito.gichtamp

import net.dankito.gichtamp.metadata.IFileMetadataReader
import net.dankito.gichtamp.metadata.MediaMetadataRetrieverFileMetadataReader
import net.dankito.gichtamp.model.*
import net.dankito.gichtamp.persistence.JacksonJsonSerializer
import net.dankito.gichtamp.search.IIndexedPlaylistItemsSearcher
import org.slf4j.LoggerFactory
import java.io.*
import kotlin.concurrent.thread


class PlaylistManager(private val dataFolder: File, private val playlistItemsSearcher: IIndexedPlaylistItemsSearcher?) {

    companion object {
        private const val PlaylistSettingsFilename = "playlist-settings.json"

        private const val PlaylistsFolderName = "playlists"

        private const val FileCharsetName = "utf-8"

        private const val NotInitializedPlaylistName = "uninitialized"

        private val log = LoggerFactory.getLogger(PlaylistManager::class.java)
    }


    private val playlistInfos = mutableSetOf<PlaylistInfo>()

    private val loadedPlaylists = mutableMapOf<PlaylistInfo, Playlist>()

    val enqueuedSongs = EnqueuedSongsList()

    val previousSongs = PreviouslyPlayedSongsList()


    private val displayedPlaylistChangedListeners = HashSet<(Playlist) -> Unit>()

    private val currentPlaylistChangedListeners = HashSet<(Playlist) -> Unit>()

    private val playlistItemsChangedListeners = HashSet<(Playlist, isDisplayedPlaylist: Boolean, isCurrentPlaylist: Boolean) -> Unit>()

    private val metadataReader: IFileMetadataReader = MediaMetadataRetrieverFileMetadataReader()

    private val serializer = JacksonJsonSerializer()


    var currentSong: Song? = null
        private set

    var currentPlaylist: Playlist = createDefaultPlaylist()
        private set

    var displayedPlaylist: Playlist = currentPlaylist
        private set

    fun isDisplayedPlaylist(playlistInfo: PlaylistInfo): Boolean {
        return playlistInfo.id == displayedPlaylist.id
    }

    val folderPlaylistInfos: List<PlaylistInfo>
        get() = playlistInfos.filter { it.type == PlaylistType.FromDirectory }

    val folderPlaylistInfosSorted: List<PlaylistInfo>
        get() = folderPlaylistInfos.sortedBy { it.name.toLowerCase() }

    val tagPlaylistInfos: List<PlaylistInfo>
        get() = playlistInfos.filter { it.type == PlaylistType.Tag }

    val tagPlaylistInfosSorted: List<PlaylistInfo>
        get() = tagPlaylistInfos.sortedBy { it.name.toLowerCase() }

    val createdPlaylistInfos: List<PlaylistInfo>
        get() = playlistInfos.filter { it.type == PlaylistType.Created }

    val createdPlaylistInfosSorted: List<PlaylistInfo>
        get() = createdPlaylistInfos.sortedBy { it.name.toLowerCase() }


    /**
     * Do not call this method before READ_EXTERNAL_STORAGE permission is granted
     */
    fun setup(defaultPlaylistFolder: File) {
        if (playlistInfos.isEmpty()) { // setup() gets also called on screen rotation -> don't load playlists again
            val settings = loadSettings(defaultPlaylistFolder)

            playlistInfos.addAll(settings.infos)

            playlistInfos.firstOrNull { settings.currentPlaylistId == it.id }?.let { currentPlaylistInfo ->
                getPlaylist(currentPlaylistInfo)?.let {
                    currentPlaylist = it
                    currentSong = currentPlaylist.currentSong
                }
            }

            playlistInfos.firstOrNull { settings.displayedPlaylistId == it.id }?.let { displayedPlaylistInfo ->
                getPlaylist(displayedPlaylistInfo)?.let {
                    displayedPlaylist = it
                }
            }
        }
    }


    fun setDisplayedPlaylist(playlistInfo: PlaylistInfo) {
        getPlaylist(playlistInfo)?.let { playlist ->
            setDisplayedPlaylist(playlist)
        }
    }

    fun setDisplayedPlaylist(playlist: Playlist) {
        displayedPlaylistChanged(playlist)
    }

    private fun displayedPlaylistChanged(displayedPlaylist: Playlist) {
        this.displayedPlaylist = displayedPlaylist

        callDisplayedPlaylistChangedListeners(displayedPlaylist)

        saveSettings()
    }


    fun setCurrentPlaylist(playlistInfo: PlaylistInfo) {
        getPlaylist(playlistInfo)?.let { playlist ->
            setCurrentPlaylist(playlist)
        }
    }

    fun setCurrentPlaylist(playlist: Playlist) {
        if (playlist != currentPlaylist) {
            currentPlaylistChanged(playlist)
        }
    }

    private fun currentPlaylistChanged(currentPlaylist: Playlist) {
        this.currentPlaylist = currentPlaylist

        callCurrentPlaylistChangedListeners(currentPlaylist)

        saveSettings()
    }

    private fun loadSettings(defaultPlaylistFolder: File): PlaylistSettings {
        val settingsFile = getSettingsFile()

        if (settingsFile.exists()) {
            try {
                return readObjectFromFile(settingsFile, PlaylistSettings::class.java)
            } catch (e: Exception) {
                log.error("Could not load PlaylistSettings from ${settingsFile.absolutePath}", e)
            }
        }

        addPlaylistFromFolder(defaultPlaylistFolder) // return default settings
        if (currentPlaylist.name == NotInitializedPlaylistName) {
            currentPlaylist = displayedPlaylist
        }
        return PlaylistSettings(playlistInfos, currentPlaylist.id, displayedPlaylist.id)
    }

    fun saveSettings() {
        val settings = PlaylistSettings(playlistInfos, currentPlaylist.id, displayedPlaylist.id)
        val settingsFile = getSettingsFile()

        try {
            writeObjectToFile(settings, settingsFile)
        } catch(e: Exception) {
            log.error("Could not save PlaylistSettings to ${settingsFile.absolutePath}", e)
        }
    }

    private fun getSettingsFile(): File {
        dataFolder.mkdirs()

        return File(dataFolder, PlaylistSettingsFilename)
    }


    fun getFolderPlaylist(info: PlaylistInfo): FolderPlaylist? {
        return getPlaylist(info) as? FolderPlaylist
    }

    fun getTagPlaylist(info: PlaylistInfo): TagPlaylist? {
        return getPlaylist(info) as? TagPlaylist
    }

    fun getCreatedPlaylist(info: PlaylistInfo): CreatedPlaylist? {
        return getPlaylist(info) as? CreatedPlaylist
    }

    private fun getPlaylist(info: PlaylistInfo): Playlist? {
        loadedPlaylists[info]?.let { return it }

        val file = getFileForPlaylist(info.id)
        if (file.exists()) {
            try {
                val type = when (info.type) {
                    PlaylistType.FromDirectory -> FolderPlaylist::class.java
                    PlaylistType.Tag -> TagPlaylist::class.java
                    PlaylistType.Created -> CreatedPlaylist::class.java
                }

                val deserializedPlaylist = readObjectFromFile(file, type)
                cachedLoadedPlaylist(info, deserializedPlaylist)

                deserializedPlaylist.initializeAfterDeserializing(info)

                if (deserializedPlaylist is FolderPlaylist) { // update songs as files in folder may have changed in the mean time
                    updateFolderPlaylistAsync(deserializedPlaylist)
                }

                return deserializedPlaylist
            } catch(e: Exception) { log.error("Could not read CreatedPlaylist $info from file ${file.absolutePath}", e)}
        }

        return null
    }

    private fun cachedLoadedPlaylist(info: PlaylistInfo, playlist: Playlist) {
        loadedPlaylists.put(info, playlist)
    }

    private fun updatePlaylistSettings(playlist: Playlist) {
        getPlaylistInfo(playlist)?.let { info ->
            info.name = playlist.name
            info.currentSongIndex = playlist.currentPlaylistPosition
            info.playSongsRandomly = playlist.playSongsRandomly
            info.countItems = playlist.size
        }
    }

    private fun getPlaylistInfo(playlist: Playlist): PlaylistInfo? {
        return playlistInfos.firstOrNull { playlist.id == it.id }
    }

    private fun savePlaylist(playlist: Playlist): Boolean {
        val file = getFileForPlaylist(playlist.id)

        try {
            writeObjectToFile(playlist, file)

            return true
        } catch(e: Exception) {
            log.error("Could not save CreatedPlaylist $playlist to file ${file.absolutePath}", e)
        }

        return false
    }

    private fun getFileForPlaylist(playlistId: String): File {
        val folder = File(dataFolder, PlaylistsFolderName)
        folder.mkdirs()

        return File(folder, "$playlistId.json")
    }


    private fun <T> readObjectFromFile(file: File, type: Class<T>): T {
        val reader = InputStreamReader(FileInputStream(file), FileCharsetName)
        val json = reader.readText()
        reader.close()

        return serializer.deserializeObject(json, type)
    }

    private fun writeObjectToFile(objectToWrite: Any, file: File) {
        val json = serializer.serializeObject(objectToWrite)

        val writer = OutputStreamWriter(FileOutputStream(file), FileCharsetName)
        writer.write(json)

        writer.flush()
        writer.close()
    }


    fun addTagPlaylist(tagName: String): TagPlaylist? {
        if (hasTagPlaylistWithName(tagName)) { // don't create a second tag with same name
            return null
        }

        val newTag = TagPlaylist(tagName)

        addTagPlaylist(newTag)

        return newTag
    }

    private fun addTagPlaylist(newTag:TagPlaylist) {
        val newPlaylistInfo = createPlaylistInfo(newTag, PlaylistType.Tag)

        addPlaylist(newPlaylistInfo, newTag)
    }

    fun addCreatedPlaylist(playlistName: String): CreatedPlaylist? {
        if (hasCreatedPlaylistWithName(playlistName)) { // don't create a second playlist with same name
            return null
        }

        val newPlaylist = CreatedPlaylist(playlistName)

        addCreatedPlaylist(newPlaylist)

        return newPlaylist
    }

    private fun addCreatedPlaylist(newPlaylist: CreatedPlaylist) {
        val newPlaylistInfo = createPlaylistInfo(newPlaylist, PlaylistType.Created)

        addPlaylist(newPlaylistInfo, newPlaylist)
    }

    private fun createPlaylistInfo(playlist: Playlist, type: PlaylistType): PlaylistInfo {
        return PlaylistInfo(playlist.id, type, playlist.name, playlist.size, playlist.currentPlaylistPosition, playlist.playSongsRandomly)
    }

    private fun addPlaylist(newPlaylistInfo: PlaylistInfo, newPlaylist: Playlist) {
        savePlaylist(newPlaylist)

        playlistInfos.add(newPlaylistInfo)
        cachedLoadedPlaylist(newPlaylistInfo, newPlaylist)

        saveSettings()
    }

    fun hasTagPlaylistWithName(tagName: String): Boolean {
        tagPlaylistInfos.forEach { info ->
            if(info.name.toLowerCase() == tagName.toLowerCase()) {
                return true
            }
        }

        return false
    }

    fun hasCreatedPlaylistWithName(playlistName: String): Boolean {
        createdPlaylistInfos.forEach { info ->
            if(info.name.toLowerCase() == playlistName.toLowerCase()) {
                return true
            }
        }

        return false
    }

    fun addPlaylistFromFolderAsync(folder: File, playlistCreated: (FolderPlaylist?) -> Unit) {
        thread {
            playlistCreated(addPlaylistFromFolder(folder))
        }
    }

    fun addPlaylistFromFolder(folder: File): FolderPlaylist? {
        if (hasFolderPlaylistWithFolder(folder)) { // don't add a second playlist with same folder
            updateFolderPlaylistAsync(folder)

            return null
        }

        val newPlaylist = FolderPlaylist(folder)

        updateFolderPlaylistAsync(newPlaylist)

        addPlaylist(createPlaylistInfo(newPlaylist, PlaylistType.FromDirectory), newPlaylist)

        displayedPlaylistChanged(newPlaylist)

        return newPlaylist
    }

    private fun hasFolderPlaylistWithFolder(folder: File): Boolean {
        return getFolderPlaylistForFolder(folder) != null
    }

    private fun getFolderPlaylistForFolder(folder: File): PlaylistInfo? {
        return folderPlaylistInfos.firstOrNull { getFolderPlaylist(it)?.playlistFolder == folder }
    }

    private fun updateFolderPlaylistAsync(folder: File) {
        thread {
            updateFolderPlaylist(folder)
        }
    }

    private fun updateFolderPlaylist(folder: File) {
        getFolderPlaylistForFolder(folder)?.let { playlistInfo ->
            getFolderPlaylist(playlistInfo)?.let { playlist ->
                updateFolderPlaylist(playlist)
            }
        }
    }

    private fun updateFolderPlaylistAsync(playlist: FolderPlaylist) {
        thread {
            updateFolderPlaylist(playlist)
        }
    }

    /**
     * Reading all songs from (large) directories, especially their metadata, takes a lot of time, so don't call this method from UI thread
     */
    private fun updateFolderPlaylist(playlist: FolderPlaylist) {
        val isRetrievingPlaylistItemsForFirstTime = playlist.songs.isEmpty()

        val files = mutableListOf<File>()
        getAllFilesInFolderRecursively(playlist.playlistFolder, files)

        // 1) do it in a two step process: First load all songs from file system and display immediate result on UI ...
        val songs = files.map { file ->
            Song(file, getRelativePathTo(file, playlist.playlistFolder))
        }

        playlist.updateSongs(songs)

        loadedPlaylists.entries.firstOrNull { it.value == playlist }?.key?.let { playlistInfo -> // update PlaylistInfo.countItems
            if (playlistInfo.countItems != songs.size) {
                playlistInfo.countItems = songs.size
                saveSettings()
            }
        }

        if (isRetrievingPlaylistItemsForFirstTime) {
            callPlaylistItemsChangedListeners(playlist)

            playlistItemsSearcher?.indexPlaylist(playlist)
        }


        // 2) ... then get their metadata - which takes a lot of time for a larger amount of files - and then display final result

        songs.forEach { song ->
            song.metadata = metadataReader.readMetadata(song.path)
        }

        playlist.updateSongs(songs)

        savePlaylist(playlist)

        callPlaylistItemsChangedListeners(playlist)

        playlistItemsSearcher?.indexPlaylist(playlist)
    }

    private fun getAllFilesInFolderRecursively(directory: File, files: MutableList<File>) {
        directory.listFiles()?.forEach { file ->
            if (file.isDirectory) {
                getAllFilesInFolderRecursively(file, files)
            }
            else {
                files.add(file)
            }
        }
    }

    // returns null if file isn't relative to folder
    private fun getRelativePathTo(file: File, folder: File): String? {
        val filePath = file.absolutePath
        val folderPath = folder.absolutePath

        return if(filePath.startsWith(folderPath)) {
            filePath.substring(folderPath.length + 1)
        }
        else {
            null
        }
    }

    fun addSongToPlaylist(playlistInfo: PlaylistInfo, song: Song) {
        getPlaylist(playlistInfo)?.let { addSongToPlaylist(it, song) }
    }

    fun addSongToPlaylist(playlist: Playlist, song: Song) {
        playlist.add(song)

        savePlaylist(playlist)
        updatePlaylistSettings(playlist)
        saveSettings()

        playlistItemsSearcher?.indexSong(playlist, song)

        callPlaylistItemsChangedListeners(playlist)
    }


    fun deletePlaylist(playlistInfo: PlaylistInfo) {
        val playlistIndex = playlistInfos.indexOf(playlistInfo)
        playlistInfos.remove(playlistInfo)

        val deletedPlaylist = loadedPlaylists.remove(playlistInfo)

        // TODO: delete playlist file!

        if (displayedPlaylist == deletedPlaylist) {
            selectPreviousPlaylist(playlistIndex)
        }

        saveSettings()
    }

    private fun selectPreviousPlaylist(playlistIndex: Int) {
        if (playlistInfos.isEmpty()) { // last playlist has been deleted
            currentPlaylistChanged(createDefaultPlaylist())
        }
        else {
            val previousPlaylistIndex = if (playlistIndex <= 0) 0 else playlistIndex - 1
            getPlaylist(playlistInfos.toList()[previousPlaylistIndex])?.let {
                currentPlaylistChanged(it)
            }
            ?: run {
                currentPlaylistChanged(createDefaultPlaylist())
            }
        }
    }

    private fun createDefaultPlaylist(): Playlist {
        return CreatedPlaylist(NotInitializedPlaylistName)
    }


    fun getTagsForSong(song: Song): List<PlaylistInfo> {
        return tagPlaylistInfosSorted.filter { doesPlaylistContainSong(it, song) }
    }

    fun doesPlaylistContainSong(playlist: PlaylistInfo, song: Song): Boolean {
        return getPlaylist(playlist)?.let { doesPlaylistContainSong(it, song) } ?: false
    }

    fun doesPlaylistContainSong(playlist: SongsList, song: Song): Boolean {
        return playlist.songs.any { it.path == song.path }
    }


    fun currentSongChanged(currentSong: Song, playlist: SongsList) {
        this.currentSong = currentSong

        previousSongs.addAtTop(currentSong)

        if (playlist is Playlist) { // filters out EnqueuedSongsList and PreviouslyPlayedSongsList
            playlist.setCurrentSong(currentSong)
            updatePlaylistSettings(playlist)

            setCurrentPlaylist(playlist)
        }
    }


    val hasEnqueuedSongs: Boolean
        get() = enqueuedSongs.hasItems

    fun getAndDequeueFirstEnqueuedSong(): Song? {
        if (hasEnqueuedSongs ) {
            return enqueuedSongs.removeFirstItem()
        }

        return null
    }

    fun enqueueSong(song: Song) {
        enqueuedSongs.add(song)

        saveSettings()
    }

    fun enqueueSongAtTop(song: Song) {
        enqueuedSongs.addAtTop(song)

        saveSettings()
    }

    private fun removeSongFromEnqueuedSongs(song: Song) {
        var isStillEnqueued = true

        while (isStillEnqueued) {
            isStillEnqueued = enqueuedSongs.removeSong(song)
        }
    }


    fun getPreviouslyPlayedSong(): Song? {
        // TODO: not fully correct: If we are already playing previously played song and then are pressing 'Prev' again, the song before the previously played song should be played
        if (previousSongs.size >= 2) {
            return previousSongs.get(1) // return second to last item as last item is current song
        }

        return null
    }


    fun toggleDisplayedPlaylistPlaysSongsRandomly() {
        displayedPlaylist.togglePlaysSongsRandomly()

        updatePlaylistSettings(displayedPlaylist)

        if (currentPlaylist != displayedPlaylist && displayedPlaylist.playSongsRandomly) {
            setCurrentPlaylist(displayedPlaylist)
        }
    }


    fun deleteFileFromPlaylistAsync(playlist: Playlist, song: Song, deletionResult: (Boolean) -> Unit) {
        thread {
            val successful = song.path.delete() // has to be done off UI thread

            removeSongFromPlaylist(playlist, song)

            callPlaylistItemsChangedListeners(playlist)

            deletionResult(successful)
        }
    }

    fun removeSongFromPlaylist(playlistInfo: PlaylistInfo, song: Song) {
        getPlaylist(playlistInfo)?.let { removeSongFromPlaylist(it, song) }
    }

    fun removeSongFromPlaylist(playlist: Playlist, song: Song) {
        playlist.removeSong(song)

        removeSongFromEnqueuedSongs(song) // just in case it was also enqueued

        savePlaylist(playlist)
        updatePlaylistSettings(playlist)

        saveSettings()

        callPlaylistItemsChangedListeners(playlist)
    }


    fun addDisplayedPlaylistChangedListener(listener: (Playlist) -> Unit) {
        displayedPlaylistChangedListeners.add(listener)
    }

    private fun callDisplayedPlaylistChangedListeners(playlist: Playlist) {
        ArrayList(displayedPlaylistChangedListeners).forEach { listener ->
            listener(playlist)
        }
    }

    fun addCurrentPlaylistChangedListener(listener: (Playlist) -> Unit) {
        currentPlaylistChangedListeners.add(listener)
    }

    private fun callCurrentPlaylistChangedListeners(playlist: Playlist) {
        ArrayList(currentPlaylistChangedListeners).forEach { listener ->
            listener(playlist)
        }
    }

    fun addPlaylistItemsChangedListener(listener: (Playlist, isDisplayedPlaylist: Boolean, isCurrentPlaylist: Boolean) -> Unit) {
        playlistItemsChangedListeners.add(listener)
    }

    private fun callPlaylistItemsChangedListeners(playlist: Playlist) {
        val isDisplayedPlaylist = playlist == displayedPlaylist
        val isCurrentPlaylist = playlist == currentPlaylist

        ArrayList(playlistItemsChangedListeners).forEach { listener ->
            listener(playlist, isDisplayedPlaylist, isCurrentPlaylist)
        }
    }

}