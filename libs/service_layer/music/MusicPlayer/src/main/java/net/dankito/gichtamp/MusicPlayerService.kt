package net.dankito.gichtamp

import android.app.Service
import android.content.Intent
import android.os.IBinder
import net.dankito.gichtamp.di.MusicPlayerComponent
import net.dankito.gichtamp.model.PlayerState
import net.dankito.gichtamp.model.PlayerStateEvent
import javax.inject.Inject


class MusicPlayerService : Service() {

    @Inject
    protected lateinit var musicPlayer: MusicPlayer

    @Inject
    protected lateinit var playlistManager: PlaylistManager

    private var binder: IBinder? = null


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    override fun onCreate() {
        super.onCreate()

        MusicPlayerComponent.component.inject(this)

        if (binder == null) {
            binder = MusicPlayerServiceBinder(this)

            musicPlayer.addStateChangedListener { event -> playerStateChanged(event) }
        }
    }

    override fun onDestroy() {
        musicPlayer.shutdown()

        super.onDestroy()
    }


    private fun playerStateChanged(event: PlayerStateEvent) {
        if (event.state == PlayerState.Playing) { // save playlist when playing next song starts (ok, also when resuming after pause, but doesn't matter)
            playlistManager.saveSettings()
        }
    }

}