package net.dankito.gichtamp.model

import java.io.File


class FolderPlaylist(val playlistFolder: File) : Playlist(playlistFolder.name) {


    internal constructor() : this(File("/")) // for object deserializers


    override fun toString(): String {
        return "$playlistFolder: $size items"
    }

    fun updateSongs(songs: List<Song>) {
        // TODO: but in this way all Song.ids get lost
        songsField.clear()

        songsField.addAll(songs)
    }

}