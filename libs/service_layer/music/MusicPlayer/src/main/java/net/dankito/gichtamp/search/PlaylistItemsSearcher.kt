package net.dankito.gichtamp.search

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.dankito.gichtamp.model.Playlist
import net.dankito.gichtamp.model.Song


open class PlaylistItemsSearcher : IPlaylistItemsSearcher {

    override fun searchAsync(playlist: Playlist, searchTerm: String, searchResult: (List<Song>) -> Unit) {
        GlobalScope.launch(Dispatchers.IO) {
            val result = search(playlist, searchTerm)

            withContext(Dispatchers.Main) {
                searchResult(result)
            }
        }
    }

    override fun search(playlist: Playlist, searchTerm: String): List<Song> {
        val lowerCaseQuery = searchTerm.toLowerCase()

        return playlist.songs.filter { song ->
            song.filename.contains(lowerCaseQuery, true)
                    || song.relativePath?.contains(lowerCaseQuery, true) == true
                    || song.metadata?.artist?.contains(lowerCaseQuery, true) == true
                    || song.metadata?.title?.contains(lowerCaseQuery, true) == true
                    || song.metadata?.album?.contains(lowerCaseQuery, true) == true
        }
    }

}