package net.dankito.gichtamp.model


enum class PlayerState {

    Initialized,
    Playing,
    Paused,
    Stopped

}