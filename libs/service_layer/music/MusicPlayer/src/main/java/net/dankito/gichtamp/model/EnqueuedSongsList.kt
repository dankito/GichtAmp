package net.dankito.gichtamp.model


class EnqueuedSongsList : SongsList() {

    fun removeFirstItem(): Song {
        val removedSong = songsField.removeAt(0)

        callPlaylistChangedListeners()

        return removedSong
    }

}