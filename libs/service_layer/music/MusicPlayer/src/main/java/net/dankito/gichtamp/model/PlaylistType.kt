package net.dankito.gichtamp.model


enum class PlaylistType {

    FromDirectory,

    Created,

    Tag

}