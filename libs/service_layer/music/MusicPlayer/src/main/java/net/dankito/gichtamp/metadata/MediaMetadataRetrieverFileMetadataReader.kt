package net.dankito.gichtamp.metadata

import android.media.MediaMetadataRetriever
import org.slf4j.LoggerFactory
import java.io.File


open class MediaMetadataRetrieverFileMetadataReader : IFileMetadataReader {

    companion object {
        private val log = LoggerFactory.getLogger(MediaMetadataRetrieverFileMetadataReader::class.java)
    }


    override fun readMetadata(file: File): Metadata? {
        val metadataRetriever = MediaMetadataRetriever()

        try {
            metadataRetriever.setDataSource(file.absolutePath)

            return Metadata(
                    extractFirstStringValue(metadataRetriever, MediaMetadataRetriever.METADATA_KEY_ARTIST, MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST),
                    extractFirstStringValue(metadataRetriever, MediaMetadataRetriever.METADATA_KEY_TITLE),
                    extractFirstStringValue(metadataRetriever, MediaMetadataRetriever.METADATA_KEY_ALBUM),
                    extractIntValue(metadataRetriever, MediaMetadataRetriever.METADATA_KEY_DURATION)
            )
        } catch (e: Exception) {
            log.error("Could not retrieve metadata of file '$file' with MediaMetadataRetriever", e)
        }
        finally {
            metadataRetriever.release()
        }

        return null
    }

    private fun extractFirstStringValue(metadataRetriever: MediaMetadataRetriever, vararg metadataKeys: Int): String {
        return extractFirstStringValue(metadataRetriever, "", *metadataKeys)
    }

    private fun extractFirstStringValue(metadataRetriever: MediaMetadataRetriever, defaultValue: String = "", vararg metadataKeys: Int): String {
        metadataKeys.forEach { metadataKey ->
            val value = metadataRetriever.extractMetadata(metadataKey)

            if (value.isNullOrBlank() == false) {
                return value.trim()
            }
        }

        return defaultValue
    }

    private fun extractIntValue(metadataRetriever: MediaMetadataRetriever, metadataKey: Int): Int? {
        try {
            val value = metadataRetriever.extractMetadata(metadataKey)

            if (value.isNullOrBlank() == false) {
                return value.trim().toInt()
            }
        } catch (e: Exception) {
            log.error("Could not get Int value for key $metadataKey", e)
        }

        return null
    }

}