package net.dankito.gichtamp.search

import net.dankito.gichtamp.model.Playlist
import net.dankito.gichtamp.model.Song


interface IPlaylistItemsSearcher {

    fun searchAsync(playlist: Playlist, searchTerm: String, searchResult: (List<Song>) -> Unit)

    fun search(playlist: Playlist, searchTerm: String): List<Song>

}