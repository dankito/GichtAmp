package net.dankito.gichtamp.model


data class PlaylistSettings(val infos: Set<PlaylistInfo>, val currentPlaylistId: String, val displayedPlaylistId: String = "") {

    internal constructor() : this(setOf(), "", "") // for object deserializers

}