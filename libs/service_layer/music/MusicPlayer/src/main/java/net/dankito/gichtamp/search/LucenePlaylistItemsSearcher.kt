package net.dankito.gichtamp.search

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.dankito.gichtamp.model.Playlist
import net.dankito.gichtamp.model.Song
import net.dankito.utils.lucene.index.DocumentsWriter
import net.dankito.utils.lucene.index.FieldBuilder
import net.dankito.utils.lucene.search.FieldMapper
import net.dankito.utils.lucene.search.QueryBuilder
import net.dankito.utils.lucene.search.SearchConfig
import net.dankito.utils.lucene.search.Searcher
import org.apache.lucene.search.Query
import org.apache.lucene.search.SortField
import org.slf4j.LoggerFactory
import java.io.File


open class LucenePlaylistItemsSearcher(context: Context) : IPlaylistItemsSearcher, IIndexedPlaylistItemsSearcher {

    companion object {
        const val SongIdFieldName = "id"

        const val SongPlaylistIdFieldName = "playlist_id"

        const val SongFilenameFieldName = "filename"
        const val SongRelativePathFieldName = "relative_path"
        const val SongPathFieldName = "path"

        const val SongArtistFieldName = "artist"
        const val SongTitleFieldName = "title"
        const val SongAlbumFieldName = "album"
        const val SongLengthFieldName = "length"

        private val log = LoggerFactory.getLogger(LucenePlaylistItemsSearcher::class.java)
    }


    protected val indexDirectory = File(context.filesDir, "playlists")


    protected val fields = FieldBuilder()

    protected val documentsWriter = DocumentsWriter(indexDirectory)

    protected val queries = QueryBuilder()

    protected val fieldMapper = FieldMapper()

    protected val searcher = Searcher(indexDirectory)


    override fun searchAsync(playlist: Playlist, searchTerm: String, searchResult: (List<Song>) -> Unit) {
        GlobalScope.launch(Dispatchers.IO) {
            val result = search(playlist, searchTerm)

            withContext(Dispatchers.Main) {
                searchResult(result)
            }
        }
    }

    override fun search(playlist: Playlist, searchTerm: String): List<Song> {
        try {
            val query = createQuery(searchTerm)

            val searchResult = searcher.search(SearchConfig(query, sortFields = listOf(SortField(SongPathFieldName, SortField.Type.STRING))))

            // TODO: not fully working, sometimes not a single id of search results is contained in songsById
            val songsById = playlist.songs.associateBy { it.id }
            return searchResult.hits.mapNotNull { result ->
                val songId = fieldMapper.string(result, SongIdFieldName)
                songsById[songId]
            }
        } catch (e: Exception) {
            log.error("Could not search for '$searchTerm'", e)
        }

        return listOf()
    }

    private fun createQuery(searchTerm: String): Query {
        val query = queries.createQueriesForSingleTerms(searchTerm.toLowerCase()) { singleTerm ->
            listOf(
                queries.fulltextQuery(SongFilenameFieldName, singleTerm),
                queries.fulltextQuery(SongFilenameFieldName, singleTerm),
                queries.fulltextQuery(SongRelativePathFieldName, singleTerm),
                queries.fulltextQuery(SongArtistFieldName, singleTerm),
                queries.fulltextQuery(SongTitleFieldName, singleTerm),
                queries.fulltextQuery(SongAlbumFieldName, singleTerm)
            )
        }
        return query
    }


    override fun indexPlaylistAsync(playlist: Playlist) {
        GlobalScope.launch(Dispatchers.IO) {
            indexPlaylist(playlist)
        }
    }

    override fun indexPlaylist(playlist: Playlist) {
        playlist.songs.forEach { song ->
            indexSong(playlist, song)
        }
    }

    override fun indexSong(playlist: Playlist, song: Song) {
        documentsWriter.updateDocumentForNonNullFields(SongIdFieldName, song.id,
                fields.fullTextSearchField(SongFilenameFieldName, song.filename, true),
                fields.nullableFullTextSearchField(SongRelativePathFieldName, song.relativePath, true),
                fields.nullableFullTextSearchField(SongArtistFieldName, song.metadata?.artist, true),
                fields.nullableFullTextSearchField(SongTitleFieldName, song.metadata?.title, true),
                fields.nullableFullTextSearchField(SongAlbumFieldName, song.metadata?.album, true),
                fields.storedField(SongPlaylistIdFieldName, playlist.id),
                fields.storedField(SongPathFieldName, song.path.absolutePath),
                fields.nullableStoredField(SongLengthFieldName, song.metadata?.length)
        )
    }

}