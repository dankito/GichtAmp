package net.dankito.gichtamp

import android.content.Context
import android.media.AudioManager
import net.dankito.gichtamp.model.PlayerState
import net.dankito.gichtamp.model.PlayerStateEvent


/**
 * To get notified if another app starts playback or of an incoming call,
 * a AudioManager.OnAudioFocusChangeListener has to be registered on AudioManager.
 * This class wraps this and its hassles.
 */
open class AudioFocusChangeListener(context: Context, protected val musicPlayer: MusicPlayer) {


    private val audioFocusChangeListener = AudioManager.OnAudioFocusChangeListener { focus -> handleAudioFocusChange(focus) }

    private val stateChangeListener: (PlayerStateEvent) -> Unit = { event ->
        handlePlayerStateEvent(event)
    }


    protected val audioManager: AudioManager

    protected var transientLossOfFocusCausedPause = false


    init {
        audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

        musicPlayer.addStateChangedListener(stateChangeListener)
    }


    open fun close() {
        audioManager.abandonAudioFocus(audioFocusChangeListener)
    }


    protected open fun handleAudioFocusChange(focus: Int) {
        when (focus) {
            AudioManager.AUDIOFOCUS_LOSS -> musicPlayer.pause()
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                if (musicPlayer.isPlaying) {
                    musicPlayer.pause()
                    transientLossOfFocusCausedPause = true
                }
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                // TODO: what to do here?
                musicPlayer.pause()
            }
            AudioManager.AUDIOFOCUS_GAIN -> {
                if (transientLossOfFocusCausedPause) {
                    transientLossOfFocusCausedPause = false
                    musicPlayer.resume()
                }
            }
        }
    }

    protected open fun handlePlayerStateEvent(event: PlayerStateEvent) {
        if (event.state == PlayerState.Playing) {
            // don't know why but has to be registered on every call to play, otherwise won't get fired anymore after first or second invocation
            audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
        }

        if (event.state != PlayerState.Paused) {
            transientLossOfFocusCausedPause = false
        }
    }

}