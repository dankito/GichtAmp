package net.dankito.gichtamp

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.PowerManager
import net.dankito.gichtamp.model.*
import org.slf4j.LoggerFactory


class MusicPlayer(private val context: Context, private val playlistManager: PlaylistManager) : MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    companion object {
        private val log = LoggerFactory.getLogger(MusicPlayer::class.java)
    }


    private val player = MediaPlayer()


    var state: PlayerState
        private set

    val isPlaying: Boolean
        get() = state == PlayerState.Playing


    /**
     * Returns the current song's duration in seconds.
     *
     * If no duration is available, e.g. if no song is selected, -1 is returned.
     */
    val durationInSeconds: Int
        get() = player.duration / 1000

    val currentPositionInSeconds: Int
        get() = player.currentPosition / 1000

    /**
     * Returns current songs position relativ to its duration as a value from 0.0 to 1.0.
     */
    val currentPositionPercent: Float
        get() {
            val duration = durationInSeconds
            if (duration < 0) {
                return 0f
            }

            return currentPositionInSeconds / duration.toFloat()
        }

    fun setCurrentPositionTo(positionInSeconds: Int) {
        player.seekTo(positionInSeconds * 1000)
    }


    private val playlist: Playlist
        get() = playlistManager.currentPlaylist

    private var stateChangedListeners: MutableList<(PlayerStateEvent) -> Unit> = ArrayList()


    // if onCompletionListener gets fired when phone is in power safe mode, MusicPlayer.prepareAsync()
    // won't fire until CPU gets turned on the next time, which can take up to a minute. Therefore
    // use WakeLock to avoid this
    private var wakeLock: PowerManager.WakeLock?


    private val audioFocusChangeListener = AudioFocusChangeListener(context, this)


    init {
        player.setWakeMode(context.applicationContext, PowerManager.PARTIAL_WAKE_LOCK)
        player.setAudioStreamType(AudioManager.STREAM_MUSIC)

        player.setOnPreparedListener(this)
        player.setOnCompletionListener(this)
        player.setOnErrorListener(this)

        state = PlayerState.Initialized

        playlistManager.addCurrentPlaylistChangedListener { currentPlaylistChanged(playlist) }

        val powerManager = context.getSystemService(Context.POWER_SERVICE) as PowerManager?
        wakeLock = powerManager?.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, javaClass.name)
        wakeLock?.setReferenceCounted(false)
    }


    fun shutdown() {
        stop()
        player.release()

        wakeLock?.release()

        audioFocusChangeListener.close()
    }


    fun playOrPause() {
        if (state == PlayerState.Playing) {
            pause()
        }
        else if (state == PlayerState.Paused) {
            resume()
        }
        else if (state == PlayerState.Initialized && playlistManager.currentSong != null) { // on start up, no song has been played yet
            playSong(playlistManager.currentSong!!, playlistManager.currentPlaylist)
        }
        else {
            playNextSong()
        }
    }

    fun playSong(song: Song, playlist: SongsList): Boolean {
        stop()
        player.reset()

        try {
            player.setDataSource(song.path.absolutePath)

            // if prepareAsync() gets called when phone is in power safe mode, onPrepared() callback
            // won't fire until CPU gets turned on the next time, which can take up to a minute. Therefore
            // use WakeLock to avoid this
            wakeLock?.acquire(30000)

            player.prepareAsync()

            playlistManager.currentSongChanged(song, playlist)

            return true // TODO: senseful? as we are preparing async
        }
        catch(e: Exception) {
            log.error("Could not set MediaPlayer's data source to ${song.path.absolutePath}", e)
        }

        return false
    }

    fun pause() {
        if(state == PlayerState.Playing && player.isPlaying) {
            player.pause()
            stateChanged(PlayerState.Paused)
        }
    }

    fun resume() {
        if(state == PlayerState.Paused) {
            player.start()
            stateChanged(PlayerState.Playing)
        }
    }

    fun stop() {
        if(state == PlayerState.Playing || state == PlayerState.Paused) {
            player.stop()
            stateChanged(PlayerState.Stopped)
        }
    }

    fun playPreviousSong() {
        // TODO: not fully correct as always returns the second to last item no matter if we are already playing the previously played song (-> the third to last item should be returned)
        playlistManager.getPreviouslyPlayedSong()?.let { previouslyPlayedSong ->
            playSong(previouslyPlayedSong, playlistManager.previousSongs)
            return
        }

        if (playlist.canPlayPreviousSong) {
            playlist.getPreviouslyPlayedSong()?.let { songToPlay ->
                playSong(songToPlay, playlist)
            }
        }
    }

    fun playNextSong() {
        if (playlistManager.hasEnqueuedSongs) {
            playlistManager.getAndDequeueFirstEnqueuedSong()?.let { songToPlay ->
                if (playSong(songToPlay, playlistManager.enqueuedSongs)) {
                    return
                }
            }
        }

        if (playlist.canPlayNextSong) {
            playlist.getNextSongToPlay()?.let { songToPlay ->
                playSong(songToPlay, playlist)
            }
        }
    }


    private fun stateChanged(newState: PlayerState, forceCallingListener: Boolean = false) {
        if(newState != state || forceCallingListener) {
            state = newState

            stateChangedListeners.forEach { it.invoke(PlayerStateEvent(newState, playlist)) }
        }
    }

    fun addStateChangedListener(listener: (PlayerStateEvent) -> Unit) {
        stateChangedListeners.add(listener)
    }


    override fun onPrepared(mediaPlayer: MediaPlayer?) {
        player.start()

        stateChanged(PlayerState.Playing)

        wakeLock?.release()
    }

    override fun onCompletion(mediaPlayer: MediaPlayer?) {
        stateChanged(PlayerState.Stopped)

        playNextSong()
    }

    override fun onError(mediaPlayer: MediaPlayer?, what: Int, extra: Int): Boolean {
        stateChanged(PlayerState.Stopped)

        val whatString = if(what == MediaPlayer.MEDIA_ERROR_SERVER_DIED) "MEDIA_ERROR_SERVER_DIED"
                        else if(what == MediaPlayer.MEDIA_ERROR_UNKNOWN) "MEDIA_ERROR_UNKNOWN"
                        else "UNKNOWN"
        val extraString = when(extra) {
            MediaPlayer.MEDIA_ERROR_IO -> "MEDIA_ERROR_IO"
            MediaPlayer.MEDIA_ERROR_MALFORMED -> "MEDIA_ERROR_MALFORMED"
            MediaPlayer.MEDIA_ERROR_UNSUPPORTED -> "MEDIA_ERROR_UNSUPPORTED"
            MediaPlayer.MEDIA_ERROR_TIMED_OUT -> "MEDIA_ERROR_TIMED_OUT"
            MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK -> "MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK"
            else -> "UNKNOWN"
        }
        log.error("An error occurred in MediaPlayer, what = $whatString, extra = $extraString")

//        if(extra == MediaPlayer.MEDIA_ERROR_IO || extra == MediaPlayer.MEDIA_ERROR_MALFORMED ||
//                extra == MediaPlayer.MEDIA_ERROR_UNSUPPORTED || extra == MediaPlayer.MEDIA_ERROR_TIMED_OUT ||
//                extra == MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK) {
//            playNextSong()
//            return true
//        }

        return false
    }


    private fun currentPlaylistChanged(playlist: Playlist) {
        if(state != PlayerState.Stopped) {
            stop()
        }
        else {
            stateChanged(PlayerState.Stopped, true)
        }
    }

}