package net.dankito.gichtamp.di

import dagger.Component
import net.dankito.gichtamp.MusicPlayerService
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(MusicPlayerModule::class))
interface MusicPlayerComponent {

    companion object {
        lateinit var component: MusicPlayerComponent
    }


    fun inject(service: MusicPlayerService)

}