package net.dankito.gichtamp.model


data class PlayerStateEvent(val state: PlayerState, val isPlaying: Boolean, val canPlayPreviousSong: Boolean, val canPlayNextSong: Boolean) {

    constructor(state: PlayerState, playlist: Playlist) : this(state, state == PlayerState.Playing, playlist.canPlayPreviousSong, playlist.canPlayNextSong)

}